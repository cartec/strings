#ifndef STRING_SPLIT_HPP
#define STRING_SPLIT_HPP

#include "string_view.hpp"

namespace strings {
class skip_empty
{
public:
  bool operator () ( string_view s ) const
  { return ! s.empty(); }
};

class allow_empty
{
public:
  bool operator () ( string_view ) const
  { return true; }
};

namespace string_split_detail {
template <typename Delimiter, typename Predicate = skip_empty>
class splitter
{
  string_view source;
  Delimiter delimiter;
  Predicate pred;

public:
  typedef string_view value_type;

  splitter( string_view s, Delimiter d, Predicate p ) :
    source( s ), delimiter( d ), pred( p ) {}

  class iterator : public std::iterator<std::forward_iterator_tag,string_view>
  {
    Delimiter delimiter;
    Predicate pred;
    string_view source;
    string_view current;

    string_view get() const
    {
      return source.substr( 0, ( current.data() ? current.data() - source.data() : source.size() ) );
    }

    void advance()
    {
      for(;;)
      {
        if ( source.empty() )
        {
          current = source;
          break;
        }
        current = delimiter.find( source );
        if ( pred( get() ) )
          break;
        if ( current.data() )
          source.remove_prefix( current.data() - source.data() + current.size() );
        else
          source.clear();
      }
    }

    void next()
    {
      do
      {
        if ( current.data() )
          source.remove_prefix( current.data() - source.data() + current.size() );
        else
          source.clear();
        if ( source.empty() )
        {
          current = source;
          break;
        }
        current = delimiter.find( source );
      }
      while ( ! pred( get() ) );
    }

  public:
    iterator( const Delimiter& d, const Predicate& p ) :
      delimiter( d ), pred( p ) {}
    iterator( const splitter& s ) :
      delimiter( s.delimiter ), pred( s.pred ),
      source( s.source )
    { advance(); }

    friend bool operator == ( const iterator& a, const iterator& b )
    { return a.source.data() == b.source.data() || ( a.source.empty() && b.source.empty() ); }
    friend bool operator != ( const iterator& a, const iterator& b )
    { return !( a == b ); }

    iterator& operator ++ () { next(); return *this; }
    iterator operator ++ ( int ) { iterator tmp( *this ); next(); return tmp; }

    string_view operator * () const { return get(); }
  };

  iterator begin() const { return iterator( *this ); }
  iterator end() const { return iterator( delimiter, pred ); }

  template <typename To> To to() const
  { return To( begin(), end() ); }

  // FIXME: can only participate in overloading if Container
  // is constructible from begin() and end().
  template <typename T, typename Allocator> operator std::vector<T, Allocator> () const
  { return to< std::vector<T, Allocator> >(); }
};
} // namespace string_split_detail

class char_delimiter
{
  char delimiter;
public:
  char_delimiter( char c ) : delimiter( c ) {}
  string_view find( string_view text ) const
  {
    string_view::size_type pos = text.find( delimiter );
    return pos == string_view::npos ? string_view() : text.substr( pos, 1 );
  }
};

class literal_delimiter
{
  string_view delimiter;
public:
  literal_delimiter( string_view d ) : delimiter( d ) {}
  string_view find( string_view text ) const
  {
    string_view::size_type pos = text.find( delimiter );
    return pos == string_view::npos ? string_view() : text.substr( pos, delimiter.size() );
  }
};

class any_of
{
  string_view delimiters;
public:
  any_of( string_view d ) : delimiters( d ) {}
  string_view find( string_view text ) const
  {
    string_view::size_type pos = text.find_first_of( delimiters );
    return pos == string_view::npos ? string_view() : text.substr( pos, 1 );
  }
};

// Maybe templatize unquoted with another delimiter type.

class any_unquoted
{
  string_view delimiters;
public:
  any_unquoted( string_view d ) : delimiters( d ) {}

  string_view find( string_view text ) const
  {
    bool last_was_backslash = false;
    bool inquote = false;

    for ( string_view::size_type pos = 0; pos < text.size() ; ++pos )
    {
      if ( ! last_was_backslash )
      {
        if ( ! inquote )
        {
          if ( delimiters.find( text[ pos ] ) != string_view::npos )
            return text.substr( pos, 1 );
        }

        if ( text[ pos ] == '"' )
          inquote = ! inquote;
      }

      last_was_backslash = text[ pos ] == '\\';
    }

    return string_view();
  }
};

class char_unquoted
{
  char delimiter;
public:
  char_unquoted( char d ) : delimiter( d ) {}

  string_view find( string_view text ) const
  {
    bool last_was_backslash = false;
    bool inquote = false;

    for ( string_view::size_type pos = 0; pos < text.size() ; ++pos )
    {
      if ( ! last_was_backslash )
      {
        if ( ! inquote )
        {
          if ( text[ pos ] == delimiter )
            return text.substr( pos, 1 );
        }

        if ( text[ pos ] == '"' )
          inquote = ! inquote;
      }

      last_was_backslash = text[ pos ] == '\\';
    }

    return string_view();
  }
};

namespace string_split_detail {
template <typename T>
struct delimiter_type
{
  typedef T type;
};

template <>
struct delimiter_type<char>
{
  typedef char_delimiter type;
};

template <>
struct delimiter_type<const char*>
{
  typedef literal_delimiter type;
};

template <>
struct delimiter_type<std::string>
{
  typedef literal_delimiter type;
};

template <>
struct delimiter_type<string_view>
{
  typedef literal_delimiter type;
};
}

inline char_unquoted unquoted( char c ) { return char_unquoted( c ); }
inline any_unquoted unquoted( string_view s ) { return any_unquoted( s ); }

template <typename Delimiter, typename Predicate>
string_split_detail::splitter<typename string_split_detail::delimiter_type<Delimiter>::type, Predicate>
split( string_view s, Delimiter d, Predicate p )
{
  typedef typename string_split_detail::delimiter_type<Delimiter>::type DType;
  return string_split_detail::splitter<DType, Predicate>( s, DType( d ), p );
}

template <typename Delimiter>
string_split_detail::splitter<typename string_split_detail::delimiter_type<Delimiter>::type, skip_empty>
split( string_view s, Delimiter d )
{
  typedef typename string_split_detail::delimiter_type<Delimiter>::type DType;
  return string_split_detail::splitter<DType, skip_empty>( s, DType( d ), skip_empty() );
}
} // namespace strings

using namespace strings;

#endif // STRING_SPLIT_HPP
