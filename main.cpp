#include "string_split.hpp"

#include <iomanip>
#include <iostream>
#include <string>
#include <cstdio>
#include <ctime>

#ifdef USE_TR1
#include <tr1/unordered_map>
#else
#include <unordered_map>
#endif

using namespace std;

namespace {
template <typename T>
inline void pretty( ostream& os, const T& t )
{ os << t; }

inline void pretty( ostream& os, string_view s )
{ os << '"' << s << '"'; }
inline void pretty( ostream& os, const string& s )
{ return pretty( os, string_view( s ) ); }
inline void pretty( ostream& os, const char* s )
{ return pretty( os, string_view( s ) ); }

inline void pretty( ostream& os, char c )
{ os << '\'' << c << '\''; }

template <typename T>
inline void pretty( ostream& os, streamsize w, const T& t )
{ os << setw(w) << t; }

inline void pretty( ostream& os, streamsize w, string_view s )
{ os << '"' << setw(w) << s << '"'; }
inline void pretty( ostream& os, streamsize w, const string& s )
{ return pretty( os, w, string_view( s ) ); }
inline void pretty( ostream& os, streamsize w, const char* s )
{ return pretty( os, w, string_view( s ) ); }

inline void pretty( ostream& os, streamsize, char c )
{ os << '\'' << c << '\''; }

template <typename T>
ostream& operator << ( ostream& os, const array_view<T>& array )
{
  typedef typename array_view<T>::const_iterator iterator;
  os << '{';
  if ( ! array.empty() )
  {
    os << ' ';
    for ( iterator p = array.begin(), e = array.end(); p != e; ++p )
    {
      if ( p != array.begin() )
        os << ", ";
      pretty( os, *p );
    }
    os << ' ';
  }
  os << '}';
  return os;
}

template <typename T, std::size_t N>
inline ostream& operator << ( ostream& os, const array<T, N>& array )
{ return os << array_view<T>( array ); }

template <typename T, typename Allocator>
inline ostream& operator << ( ostream& os, const vector<T, Allocator>& v )
{ return os << array_view<T>( v ); }

class timer
{
  unsigned long long ticks;
public:
  timer(): ticks( clock() ) {}
  void stop() { ticks = clock() - ticks; }
  unsigned long long elapsed() { return ticks * 1000 / CLOCKS_PER_SEC; }
};

void time_conversions()
{
  unsigned long long n = 1000000;
  {
    string_view pi_string = "3.1415926535897932384626433";
    long double total = 0;
    timer t;
    for ( unsigned long long i = 0; i < n; ++i )
    {
      size_t pos;
      total += stod(pi_string, &pos);
    }
    t.stop();
    cout << "string_view: total = " << total << ", avg = " << total / n << ", elapsed = " << t.elapsed() << " ms\n";
  }

  {
    string pi_string = "3.1415926535897932384626433";
    long double total = 0;
    timer t;
    for ( unsigned long long i = 0; i < n; ++i )
    {
      size_t pos;
      total += stod(pi_string, &pos);
    }
    t.stop();
    cout << "string: total = " << total << ", avg = " << total / n << ", elapsed = " << t.elapsed() << " ms\n";
  }

  {
    const char* pi_string = "3.1415926535897932384626433";
    long double total = 0;
    timer t;
    for ( unsigned long long i = 0; i < n; ++i )
    {
      total += atof(pi_string);
    }
    t.stop();
    cout << "atof: total = " << total << ", avg = " << total / n << ", elapsed = " << t.elapsed() << " ms\n";
  }

  {
    const char* pi_string = "3.1415926535897932384626433";
    long double total = 0;
    timer t;
    for ( unsigned long long i = 0; i < n; ++i )
    {
      char *end;
      total += strtod(pi_string, &end);
    }
    t.stop();
    cout << "strtod: total = " << total << ", avg = " << total / n << ", elapsed = " << t.elapsed() << " ms\n";
  }
}

void time_splitting()
{
  string source = "012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789$";
  for ( int i = 0; i < 10; ++i )
    source += source;
  cout << "source.size() = " << source.size() << endl;

  unsigned long long n = 100000;
  {
    unsigned long long nsplits = 0;
    vector<string> splits;
    timer t;
    for ( unsigned long long i = 0; i < n; ++i )
    {
      splits.clear();
      const string& str = source;
      string::size_type last = 0;

      for (;;)
      {
        string::size_type pos = str.find( '$', last );
        if ( pos == string::npos )
          break;
        splits.push_back( str.substr( last, pos - last ) );
        last = pos + 1;
      }

      nsplits += splits.size();
    }
    t.stop();
    cout << "string: splits = " << nsplits << ", elapsed = " << t.elapsed() << " ms\n";
  }

  {
    unsigned long long nsplits = 0;
    vector<string_view> splits;
    timer t;
    for ( unsigned long long i = 0; i < n; ++i )
    {
      string_view str = source;

      splits = split( str, '$' );

      nsplits += splits.size();
    }
    t.stop();
    cout << "string_view: splits = " << nsplits << ", elapsed = " << t.elapsed() << " ms\n";
  }
}
} // anonymous namespace

string_view strip( string_view s )
{
  static const char space[] = " \t\n\r\v";
  string_view::size_type first = s.find_first_not_of( space );
  if ( first == string_view::npos )
    return string_view();
  return s.substr( first, s.find_last_not_of( space ) - first + 1 );
}

string_view test_static()
{ return string_view("poop").substr( 1, 2 ); }

string_view test_char_pointer( const char* str )
{ return str; }

string_view test_string( const string& str )
{ return str; }

char test_at( string_view str )
{ return str.at(3); }

char test_indexing( string_view str )
{ return str[3]; }

size_t test_hash()
{ return string_view("foobar").hash(); }

int main()
{
  cout << boolalpha;
#define REPORT(expr) do { \
  cout << #expr " = "; \
  try { \
    pretty( cout, expr ); \
  } catch( std::exception& e ) { \
    cout << "Exception: \"" << e.what() << '"'; \
  } \
  cout << '\n'; \
  } while( false )
#define REPORTW(w,expr) do { \
  cout << "setw(" #w ") << " #expr " = "; \
  try { \
    pretty( cout, w, expr ); \
  } catch( std::exception& e ) { \
    cout << "Exception: \"" << e.what() << '"'; \
  } \
  cout << '\n'; \
  } while( false )
  array<int,5> raw_ints = {{ 0, 1, 2, 3, 4 }};
  array_view<int> some_ints(raw_ints);
  REPORT(some_ints);
  REPORT(some_ints.slice(2,2));
  string_view ref("Hello World!");
  REPORT(ref);
  REPORT(string(ref));
  REPORT(std::vector<char>(ref));
  REPORT(ref.substr( 3, 5 ));
  REPORT(ref.starts_with( "Hello" ));
  REPORT(ref.ends_with( "World?" ));
  REPORT(ref.find(ref.substr(4,2)));
  REPORT(string(ref.rbegin(), ref.rend()));
  REPORT(ref.hash());

  REPORT(ref.rfind('d'));
  REPORT(string_view("01234").rfind('0'));
  REPORT(string_view("01234").rfind('2'));
  REPORT(string_view("01234").rfind('4'));
  REPORT(string_view("01234").rfind('7'));

  REPORT(strip("  xxx yyy  "));
  REPORT(strip("       "));
  REPORT(strip(""));

  string_view sub_ref;
  REPORT(sub_ref=ref.substr(1,ref.size() - 3));
  REPORT(sub_ref.to_vector( std::allocator<char>() ));
  REPORT(sub_ref.to_string< std::allocator<char> >());
  printf( "string(sub_ref).c_str(): \"%s\"\n", string(sub_ref).c_str() );
  printf( "%%.*s: \"%.*s\"\n", int(sub_ref.size()), sub_ref.data() );

  REPORT(sub_ref.at(3));
  REPORT(sub_ref.at(13));

  REPORTW(32, sub_ref);
  REPORT(sub_ref.hash());

  REPORT(sub_ref.find_first_of(""));
  REPORT(sub_ref.find_first_of(sub_ref));
  REPORT(sub_ref.find_first_of("oel"));

  REPORT(sub_ref.find_first_not_of(""));
  REPORT(sub_ref.find_first_not_of(sub_ref));
  REPORT(sub_ref.find_first_not_of("oel"));

  REPORT(sub_ref.find_last_of(""));
  REPORT(sub_ref.find_last_of(sub_ref));
  REPORT(sub_ref.find_last_of("oel"));

  REPORT(sub_ref.find_last_not_of(""));
  REPORT(sub_ref.find_last_not_of(sub_ref));
  REPORT(sub_ref.find_last_not_of("oel"));

  std::size_t pos = 0;
  REPORT(stoull(string_view("0xffffffffffffffff"), &pos, 16));
  REPORT(string_view("0xffffffffffffffff").substr(pos));

  pos = 0;
  REPORT(stoi(wstring_view(L"0xffff").substr(0,4), &pos, 16));
  REPORT(wstring_view(L"0xffff").substr(0,4).substr(pos));

  pos = 0;
  REPORT(stol(string_view("-0777foo"), &pos));
  REPORT(string_view("-0777foo").substr(pos));

  pos = 0;
  REPORT(stol(string_view("-0777bar"), &pos, 0));
  REPORT(string_view("-0777bar").substr(pos));

  cout.precision(15);
  pos = 0;
  REPORT(stod(string_view("3.1415926535"), &pos));
  REPORT(string_view("3.1415926535").substr(pos));

  pos = 0;
  REPORT(stold(string_view("3.1415926535e50"), &pos));
  REPORT(string_view("3.1415926535e50").substr(pos));

  pos = 0;
  REPORT(stold(string_view("-3.1415926535e50").substr(0,15), &pos));
  REPORT(string_view("-3.1415926535e50").substr(0,15).substr(pos));

  pos = 0;
  REPORT(stof(string_view("-inf"), &pos));
  REPORT(string_view("-inf").substr(pos));

  pos = 0;
  REPORT(stod(string_view("NaN(foo)bar"), &pos));
  REPORT(string_view("NaN(foo)bar").substr(pos));

  pos = 0;
  REPORT(stod(wstring_view(L"1.414foo"), &pos));
  REPORT(wstring_view(L"1.414foo").substr(pos));

  pos = 0;
  REPORT(stold(string_view("3.141592653589793"), &pos));
  REPORT(string_view("3.141592653589793").substr(pos));

  REPORT(vector<string_view>(split(":a:", ':', allow_empty())));
  REPORT(vector<string_view>(split("abc::::123::x::", "::", allow_empty())));
  REPORT(vector<string_view>(split("a:b;c:d;e", strings::any_of(";:"))));
  REPORT(vector<string_view>(split("a b\\ c \"d e\" f \\\"g h", unquoted(' '))));

  unordered_map<string_view, string_view> m;
  REPORT(m["foo"] = "bar");
  REPORT(m["foo"]);

  REPORT(string_view("foo") == "foo");

#undef REPORT

  time_conversions();
  time_splitting();
}

int test_assume( int n )
{
  assume( n == 4 );
  return n;
}
