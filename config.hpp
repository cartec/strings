#ifndef CONFIG_HPP
#define CONFIG_HPP

#include <cassert>

#define GCC_VERSION(major,minor) (defined(__GNUC__) && ((100 * __GNUC__ + __GNUC_MINOR__) >= 100 * (major) + (minor)))
#define GCC_CXX_VERSION(major,minor) (defined(__GXX_EXPERIMENTAL_CXX0X__) && GCC_VERSION((major),(minor)))

#if defined(USE_TR1) || (__cplusplus < 201103L && (_MSC_VER < 1600) && ! GCC_CXX_VERSION(4,4))
#undef USE_TR1
#define USE_TR1
// TR1 namespace hack.
namespace std { namespace tr1 {} using namespace tr1; }
#endif

#if defined(USE_CONSTEXPR) || __cplusplus >= 201103L || GCC_CXX_VERSION(4,6)
#undef USE_CONSTEXPR
#define USE_CONSTEXPR
#define CONSTEXPR constexpr
#else
#define CONSTEXPR
#endif

#if defined(USE_NOEXCEPT) || __cplusplus >= 201103L || GCC_CXX_VERSION(4,6)
#undef USE_NOEXCEPT
#define USE_NOEXCEPT
#define NOEXCEPT noexcept
#else
#define NOEXCEPT
#endif

#if defined(USE_DEFAULT_DELETE) || __cplusplus >= 201103L || GCC_CXX_VERSION(4,4)
#undef USE_DEFAULT_DELETE
#define USE_DEFAULT_DELETE
#endif

#if !defined(USE_STANDARD_LAYOUT) && (__cplusplus >= 201103L || (_MSC_VER >= 1700) || GCC_CXX_VERSION(4,5))
#define USE_STANDARD_LAYOUT
#endif

#ifdef __GNUC__
#define likely(x)   __builtin_expect((x),1)
#define unlikely(x) __builtin_expect((x),0)
#else
#define likely(x)   (x)
#define unlikely(x) (x)
#endif

#ifdef NDEBUG
#ifdef _MSC_VER
#define assume(cond) __assume(cond)
#elif GCC_VERSION(4,5)
#define assume(cond) ((void)((cond) || (__builtin_unreachable(),0)))
#else
#define assume(x)
#endif
#else
#define assume(x)    assert(x)
#endif

#endif // CONFIG_HPP
