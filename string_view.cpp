#include "string_view.hpp"

#include <cmath>
#include <cstdlib>
#include <cstring>
#include <limits>

#ifdef USE_TR1
#include <tr1/type_traits>
#else
#include <type_traits>
#endif

// Code common to integer and floating-point conversions

namespace {
template <typename charT>
bool is_not_space( charT c )
{
  return ! ( c == charT(' ')  || c == charT('\r') ||
             c == charT('\n') || c == charT('\t') );
}

template <typename InputIterator>
InputIterator skip_space( InputIterator first, InputIterator last )
{
  typedef typename std::iterator_traits<InputIterator>::value_type charT;
  return std::find_if( first, last, is_not_space<charT> );
}

template <typename T, bool=(std::is_signed<T>::value|std::is_floating_point<T>::value)>
struct sign_traits
{
  bool is_negative;

  template <typename InputIterator>
  sign_traits( InputIterator& first, InputIterator last ) : is_negative( false )
  {
    typedef typename std::iterator_traits<InputIterator>::value_type charT;
    if ( first != last )
    {
      is_negative = ( *first == charT('-') );
      if ( is_negative || *first == charT('+') )
        ++first;
    }
  }

  template<typename U>
  T apply( U value ) const
  {
    T v = static_cast<T>( value );
    return is_negative ? -v : v;
  }
};

template <typename T>
struct sign_traits<T, false>
{
  template <typename InputIterator>
  sign_traits( InputIterator&, InputIterator ) {}

  template <typename U>
  T apply( U value ) const
  { return static_cast<T>( value ); }
};

} // anonymous namespace


// Integer conversions

namespace {
template <typename InputIterator>
InputIterator parse_ull( InputIterator first, InputIterator last, unsigned long long& ull, unsigned base )
{
  assume( base == 0 || ( base >= 2 && base <= 36 ) );
  typedef typename std::iterator_traits<InputIterator>::value_type charT;

  if ( first != last )
  {
    if ( base == 0 )
    {
      base = 10;
      if ( *first == charT('0') )
      {
        ++first;
        base = 8;

        if ( first != last && ( *first == charT('x') || *first == charT('X') ) )
        {
          ++first;
          base = 16;
        }
      }
    }
    else if ( base == 8 )
    {
      if ( *first == charT('0') )
        ++first;
    }
    else if ( base == 16 )
    {
      if ( *first++ == charT('0') && first != last && ( *first == charT('x') || *first == charT('X') ) )
        ++first;
    }
  }

  unsigned long long value = 0;

  for ( ; first != last ; ++first )
  {
    charT c = *first;
    charT offset;
    if ( charT('0') <= c && c <= charT('9') )
      offset = charT('0');
    else if ( charT('a') <= c && c <= charT('z') )
      offset = charT('a') - 10;
    else if ( charT('A') <= c && c <= charT('Z') )
      offset = charT('A') - 10;
    else
      break;
    unsigned digit = static_cast<unsigned>( c - offset );
    if ( digit >= base )
      break;

    if ( unlikely( value > std::numeric_limits<unsigned long long>::max() / base ) )
      throw std::out_of_range("Integer conversion overflow");

    value = value * base + digit;
  }

  ull = value;
  return first;
}

template <typename T, typename InputIterator>
InputIterator parse_integer( InputIterator first, InputIterator last, int base, T& value )
{
  first = skip_space( first, last );

  sign_traits<T> sign( first, last );

  unsigned long long ull;
  first = parse_ull( first, last, ull, base );

  if ( unlikely( static_cast<unsigned long long>( static_cast<T>( ull ) ) != ull ) )
    throw std::out_of_range("Integer conversion overflow");

  value = sign.apply( ull );
  return first;
}

template <typename T, typename S>
T sto_int( const S& s, std::size_t* index, int base )
{
  T value;
  typename S::const_iterator end = parse_integer( s.begin(), s.end(), base, value );
  if ( index )
    *index = ( end == s.end() ? S::npos : end - s.begin() );
  return value;
}
} // anonymous namespace

int stoi( const string_view& s, std::size_t* index, int base )
{ return sto_int<int>( s, index, base ); }

long stol( const string_view& s, std::size_t* index, int base )
{ return sto_int<long>( s, index, base ); }

unsigned long stoul( const string_view& s, std::size_t* index, int base )
{ return sto_int<unsigned long>( s, index, base ); }

long long stoll( const string_view& s, std::size_t* index, int base )
{ return sto_int<long long>( s, index, base ); }

unsigned long long stoull( const string_view& s, std::size_t* index, int base )
{ return sto_int<unsigned long long>( s, index, base ); }


// Floating-point conversions

namespace {
template <typename InputIterator>
InputIterator parse_ld( InputIterator first, InputIterator last, long double& ld )
{
  typedef typename std::iterator_traits<InputIterator>::value_type charT;

  if ( first != last && ( *first == charT('i') || *first == charT('I') ) )
  {
    if ( ++first != last && ( *first == charT('n') || *first == charT('N') ) )
    {
      if ( ++first != last && ( *first == charT('f') || *first == charT('F') ) )
      {
        ++first;
        ld = std::numeric_limits<long double>::infinity();
        return first;
      }
    }

    throw std::invalid_argument("Invalid input to floating-point conversion");
  }

  if ( first != last && ( *first == charT('n') || *first == charT('N') ) )
  {
    if ( ++first != last && ( *first == charT('a') || *first == charT('A') ) )
    {
      if ( ++first != last && ( *first == charT('n') || *first == charT('N') ) )
      {
        if ( ++first != last && *first == charT('(') )
        {
          // Skip optional "([A-Za-z0-9]*)"
          while( first != last && *first++ != charT(')') )
            ;
        }
        ld = std::numeric_limits<long double>::quiet_NaN();
        return first;
      }
    }

    throw std::invalid_argument("Invalid input to floating-point conversion");
  }

  long double value = 0;

  for (; first != last; ++first )
  {
    charT c = *first;
    if ( ! ( charT('0') <= c && c <= charT('9') ) )
      break;

    if ( unlikely( value > std::numeric_limits<long double>::max() / 10 ) )
      throw std::out_of_range("Floating-point conversion overflow");

    value = value * 10 + ( c - charT('0') );
  }

  if ( first != last && *first == charT('.') )
  {
    long double power = 1;
    while ( ++first != last )
    {
      charT c = *first;
      if ( ! ( charT('0') <= c && c <= charT('9') ) )
        break;

      power /= 10;
      value += power * ( c - charT('0') );
    }
  }

  if ( first != last && ( *first == charT('e') || *first == charT('E') ) )
  {
    ++first;
    int exponent;
    first = parse_integer<int>( first, last, 10, exponent );
    value *= std::pow( static_cast<long double>( 10 ), exponent );
  }

  ld = value;
  return first;
}

template <typename T, typename InputIterator>
InputIterator parse_float( InputIterator first, InputIterator last, T& value )
{
  first = skip_space( first, last );

  sign_traits<T> sign( first, last );

  long double ld;
  first = parse_ld( first, last, ld );

  if ( ld == ld && unlikely( static_cast<T>( ld ) != ld ) )
    throw std::out_of_range("Floating-point conversion overflow");

  value = sign.apply( ld );
  return first;
}

template <typename T, typename S>
T sto_float( const S& s, std::size_t* index )
{
  T value;
  typename S::const_iterator end = parse_float( s.begin(), s.end(), value );
  if ( index )
    *index = ( end == s.end() ? S::npos : end - s.begin() );
  return value;
}
} // anonymous namespace

float stof( const string_view& s, std::size_t* index )
{ return sto_float<float>( s, index ); }

double stod( const string_view& s, std::size_t* index )
{ return sto_float<double>( s, index ); }

long double stold( const string_view& s, std::size_t* index )
{ return sto_float<long double>( s, index ); }

int stoi( const wstring_view& s, std::size_t* index, int base )
{ return sto_int<int>( s, index, base ); }

long stol( const wstring_view& s, std::size_t* index, int base )
{ return sto_int<long>( s, index, base ); }

unsigned long stoul( const wstring_view& s, std::size_t* index, int base )
{ return sto_int<unsigned long>( s, index, base ); }

long long stoll( const wstring_view& s, std::size_t* index, int base )
{ return sto_int<long long>( s, index, base ); }

unsigned long long stoull( const wstring_view& s, std::size_t* index, int base )
{ return sto_int<unsigned long long>( s, index, base ); }

float stof( const wstring_view& s, std::size_t* index )
{ return sto_float<float>( s, index ); }

double stod( const wstring_view& s, std::size_t* index )
{ return sto_float<double>( s, index ); }

long double stold( const wstring_view& s, std::size_t* index )
{ return sto_float<long double>( s, index ); }
