#ifndef ARRAY_VIEW_HPP
#define ARRAY_VIEW_HPP

#include <algorithm>
#include <cstddef>
#include <functional>
#include <iterator>
#include <limits>
#include <stdexcept>
#include <string>
#include <vector>

#include "config.hpp"

#ifdef USE_TR1
#include <tr1/array>
#include <tr1/functional>
#else
#include <array>
#endif

namespace array_view_detail {
template <typename T>
class rep_2ptr
{
public:
  typedef const T* pointer;
  typedef std::size_t size_type;
  typedef std::ptrdiff_t difference_type;

  CONSTEXPR rep_2ptr( pointer ptr, size_type n ) NOEXCEPT :
    first( ptr ),
    last( ptr + (assume(n == 0 || ptr != 0), n) ) {}

#ifdef USE_DEFAULT_DELETE
  CONSTEXPR rep_2ptr( const rep_2ptr& other ) NOEXCEPT = default;
  rep_2ptr& operator = ( const rep_2ptr& other ) NOEXCEPT = default;
#else
  CONSTEXPR rep_2ptr( const rep_2ptr& other ) NOEXCEPT :
    first( other.first ),
    last( other.last ) {}
  rep_2ptr& operator = ( const rep_2ptr& other ) NOEXCEPT {
    first = other.first;
    last = other.last;
  }
#endif

  CONSTEXPR bool empty() const NOEXCEPT { return first == last; }
  CONSTEXPR size_type size() const NOEXCEPT { return last - first; }

  CONSTEXPR pointer begin() const NOEXCEPT { return first; }
  CONSTEXPR pointer end() const NOEXCEPT { return last; }

  void pop_front() NOEXCEPT { ++first; }
  void pop_back() NOEXCEPT { --last; }

private:
  pointer first, last;
};

#ifdef USE_STANDARD_LAYOUT
static_assert( std::is_standard_layout< rep_2ptr<char> >::value,
               "array_view representations must be standard layout.");
#endif

template <typename T, typename Size=std::size_t, typename Difference=std::ptrdiff_t>
class rep_ptr_size
{
public:
  typedef const T* pointer;
  typedef Size size_type;
  typedef Difference difference_type;

  CONSTEXPR rep_ptr_size( pointer p, size_type n ) NOEXCEPT :
    ptr( p ), count( n )
  {}

#ifdef USE_DEFAULT_DELETE
  CONSTEXPR rep_ptr_size( const rep_ptr_size& other ) NOEXCEPT = default;
  rep_ptr_size& operator = ( const rep_ptr_size& other ) NOEXCEPT = default;
#else
  CONSTEXPR rep_ptr_size( const rep_ptr_size& other ) NOEXCEPT :
    ptr( other.ptr ), count( other.count ) {}
  rep_ptr_size& operator = ( const rep_ptr_size& other ) NOEXCEPT {
    ptr = other.ptr;
    count = other.count;
  }
#endif

  CONSTEXPR bool empty() const NOEXCEPT { return count == 0; }
  CONSTEXPR size_type size() const NOEXCEPT { return count; }

  CONSTEXPR pointer begin() const NOEXCEPT { return ptr; }
  CONSTEXPR pointer end() const NOEXCEPT { return ptr + count; }

  void pop_front() NOEXCEPT { ++ptr; --count; }
  void pop_back() NOEXCEPT { --count; }

private:
  pointer ptr;
  size_type count;
};

#ifdef USE_STANDARD_LAYOUT
static_assert( std::is_standard_layout< rep_ptr_size<char> >::value,
               "array_view representations must be standard layout." );
#endif

template <typename T>
struct rep
#if 1
{ typedef rep_ptr_size<T, std::size_t, std::ptrdiff_t> type; }; // Normal size
#else
{ typedef rep_2ptr<T> type; };
{ typedef rep_ptr_size<T, unsigned int, int> type; }; // Force 32-bit size
#endif
} // namespace array_view_detail

template <typename T, typename Rep=typename array_view_detail::rep<T>::type>
class array_view : private Rep
{
  typedef Rep base;

public:
  typedef T value_type;
  typedef const T* const_pointer;
  typedef const_pointer pointer;
  typedef const T& const_reference;
  typedef const_reference reference;
  typedef pointer const_iterator;
  typedef std::reverse_iterator<const_iterator> const_reverse_iterator;
  typedef const_reverse_iterator reverse_iterator;
  typedef const_iterator iterator;
  typedef typename base::size_type size_type;
  typedef typename base::difference_type difference_type;

protected:
  CONSTEXPR bool check_in_range( size_type pos ) const NOEXCEPT
  { return likely( pos < size() ); }

public:
  CONSTEXPR array_view( const T* ptr, size_type n ) NOEXCEPT :
    base( ptr, n ) {}

  CONSTEXPR array_view() NOEXCEPT : base( 0, 0 ) {}

  template <std::size_t N> CONSTEXPR array_view( const T (&array)[N] ) NOEXCEPT :
    base( array, (assume( N <= std::numeric_limits<size_type>::max()), N) ) {}

  template <std::size_t N> CONSTEXPR array_view( const std::array<T, N>& array ) NOEXCEPT :
    base( array.data(), (assume( N <= std::numeric_limits<size_type>::max()), N) ) {}

  template <typename Allocator> array_view( const std::vector<T, Allocator>& vec ) :
    base( vec.data(), vec.size() ) {}

  template <typename traits, typename Allocator> array_view( const std::basic_string<T, traits, Allocator>& s ) :
    base( s.data(), s.size() ) {}

  template <typename Allocator>
  std::vector<T, Allocator> to_vector( Allocator a = Allocator() ) const
  { return std::vector<T, Allocator>( begin(), end(), a ); }
  std::vector<T> to_vector() const
  { return std::vector<T>( begin(), end() ); }

  template <typename Allocator> operator std::vector<T, Allocator> () const
  { return to_vector<Allocator>(); }

  std::basic_string<T> to_string() const
  { return std::basic_string<T>( begin(), end() ); }
  template <typename traits> std::basic_string<T, traits> to_string() const
  { return std::basic_string<T, traits>( begin(), end() ); }
  template <typename traits, typename Allocator>
  std::basic_string<T, traits, Allocator> to_string( Allocator a = Allocator() ) const
  { return std::basic_string<T, traits, Allocator>( begin(), end(), a ); }

  CONSTEXPR const_iterator begin() const NOEXCEPT { return base::begin(); }
  CONSTEXPR const_iterator end() const NOEXCEPT { return base::end(); }
  CONSTEXPR const_iterator cbegin() const NOEXCEPT { return begin(); }
  CONSTEXPR const_iterator cend() const NOEXCEPT { return end(); }

  CONSTEXPR const_reverse_iterator rbegin() const NOEXCEPT { return const_reverse_iterator( end() ); }
  CONSTEXPR const_reverse_iterator rend() const NOEXCEPT { return const_reverse_iterator( begin() ); }
  CONSTEXPR const_reverse_iterator crbegin() const NOEXCEPT { return rbegin(); }
  CONSTEXPR const_reverse_iterator crend() const NOEXCEPT { return rend(); }

  CONSTEXPR size_type size() const NOEXCEPT { return base::size(); }
  CONSTEXPR size_type max_size() const NOEXCEPT { return size(); }
  CONSTEXPR size_type capacity() const NOEXCEPT { return size(); }
  CONSTEXPR bool empty() const NOEXCEPT { return base::empty(); }

  CONSTEXPR reference operator [] ( size_type pos ) const NOEXCEPT
  { return (assume( check_in_range( pos ) ), *( begin() + pos )); }
  CONSTEXPR reference at( size_type pos ) const
  {
    return *(begin() + (check_in_range( pos ) ? pos : (throw std::out_of_range( "array_view index out of range" ),0)));
  }

  CONSTEXPR reference front() const NOEXCEPT { return (assume( ! empty() ), *begin()); }
  CONSTEXPR reference back() const NOEXCEPT { return (assume( ! empty() ), *( end() - 1 )); }
  CONSTEXPR pointer data() const NOEXCEPT { return base::begin(); }

  CONSTEXPR array_view slice( size_type pos, size_type n ) const NOEXCEPT
  {
    return (assume( pos <= size() ),
            assume( pos + n <= size() ),
            array_view( begin() + pos, n ));
  }

  size_type hash() const;

  void clear() NOEXCEPT { *this = array_view(); }
  void remove_prefix( size_type n )
  {
    n = std::min( size(), n );
    *this = slice( n, size() - n );
  }
  void remove_suffix( size_type n )
  {
    n = std::min( size(), n );
    *this = slice( 0, size() - n );
  }
  void pop_front() NOEXCEPT { assume( ! empty() ); base::pop_front(); }
  void pop_back() NOEXCEPT { assume( ! empty() ); base::pop_back(); }
};

template <typename T, typename Allocator>
inline array_view<T> make_array_view( const std::vector<T, Allocator>& v )
{ return array_view<T>( v ); }

template <typename T, std::size_t N>
inline CONSTEXPR array_view<T> make_array_view( const std::array<T, N>& a )
{ return array_view<T>( a ); }

template <typename T, std::size_t N>
inline CONSTEXPR array_view<T> make_array_view( const T (&a)[N] )
{ return array_view<T>( a ); }

template <typename T>
inline CONSTEXPR array_view<T> make_array_view( const T* ptr, std::size_t n )
{ return array_view<T>( ptr, n ); }

template <typename charT, typename traits, typename Allocator>
inline array_view<charT> make_array_view( const std::basic_string<charT, traits, Allocator>& s )
{ return array_view<charT>( s ); }

namespace array_view_detail {
#ifdef USE_CONSTEXPR
inline CONSTEXPR std::size_t
hash_characters( array_view<unsigned char> array, std::size_t hash = 5381 )
{
  // Stolen from Folly and adapted to constexpr.
  return array.empty() ? hash : hash_characters( array.slice( 1, array.size() - 1 ),
                                                 33 * hash + array.front() );
}
#else
std::size_t hash_characters( array_view<unsigned char> array );
#endif
} // namespace array_view_detail

template <typename T, typename Rep>
typename array_view<T, Rep>::size_type
array_view<T, Rep>::hash() const
{
  // FIXME: This is a horrible hash function.
  size_type value = 0;
  for ( const_iterator pos = begin(), e = end(); pos != e; ++pos )
    value ^= std::hash<T>()( *pos );
  return value;
}

template <>
inline array_view<char>::size_type
array_view<char>::hash() const
{ return array_view_detail::hash_characters( make_array_view( reinterpret_cast<const unsigned char*>( begin() ), size() ) ); }

template <>
inline array_view<signed char>::size_type
array_view<signed char>::hash() const
{ return array_view_detail::hash_characters( make_array_view( reinterpret_cast<const unsigned char*>( begin() ), size() ) ); }

template <>
inline array_view<unsigned char>::size_type
array_view<unsigned char>::hash() const
{ return array_view_detail::hash_characters( *this ); }

namespace std {
#ifdef USE_TR1
namespace tr1 {
#endif
template <typename T> struct hash< array_view<T> > {
  std::size_t operator () ( array_view<T> array ) const
  { return array.hash(); }
};
#ifdef USE_TR1
} // namespace tr1
#endif
} // namespace std

#endif // ARRAY_VIEW_HPP
