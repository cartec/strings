strings
=======

Implementations of the C++ standard proposals for `string_ref` (here called `string_view`) and a string
splitting facility `split(string_view,...)`.
