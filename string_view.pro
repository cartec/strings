TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle qt

QMAKE_CXXFLAGS_RELEASE += -DNDEBUG

win32-msvc2010 {
  QMAKE_CXXFLAGS += -arch:SSE2 -fp:fast -GF
  QMAKE_CXXFLAGS_RELEASE -= -O2
  QMAKE_CXXFLAGS_RELEASE += -Ox -GL
  QMAKE_LFLAGS_RELEASE += /LTCG
}

win32-g++ {
  QMAKE_CXXFLAGS += -msse2 -mfpmath=sse -ffast-math
  QMAKE_CXXFLAGS_RELEASE -= -O2
  QMAKE_CXXFLAGS_RELEASE += -O3 -fomit-frame-pointer
  CONFIG += g++11
}

HEADERS += string_view.hpp \
    array_view.hpp \
    string_split.hpp \
    config.hpp

SOURCES += main.cpp \
    string_view.cpp \
    array_view.cpp

g++11 {
  QMAKE_CXXFLAGS += -std=c++11
  QMAKE_LFLAGS += -std=c++11
}
