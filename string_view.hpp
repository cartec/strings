#ifndef STRING_VIEW_HPP
#define STRING_VIEW_HPP

#include <algorithm>
#include <cstddef>
#include <cstring>
#include <functional>
#include <iostream>
#include <limits>
#include <stdexcept>
#include <string>

#include "config.hpp"
#include "array_view.hpp"

namespace string_view_detail {
template <typename traits>
inline CONSTEXPR std::size_t strlen( const typename traits::char_type* s, std::size_t n = 0 )
{ return traits::eq( *s, typename traits::char_type() ) ? n : strlen<traits>( s + 1, n + 1 ); }

template <typename T>
struct identity {
  typedef T type;
};
} // namespace string_view_detail

template <typename charT, typename traits = std::char_traits<charT> >
class basic_string_view : public array_view<charT>
{
  typedef array_view<charT> base;

public:
  typedef typename base::value_type value_type;
  typedef typename base::pointer pointer;
  typedef typename base::const_reference const_reference;
  typedef typename base::reference reference;
  typedef typename base::const_iterator const_iterator;
  typedef typename base::const_reverse_iterator const_reverse_iterator;
  typedef typename base::reverse_iterator reverse_iterator;
  typedef typename base::iterator iterator;
  typedef typename base::size_type size_type;
  typedef typename base::difference_type difference_type;

  static const size_type npos = size_type(-1);

  CONSTEXPR basic_string_view() NOEXCEPT : base() {}
  CONSTEXPR basic_string_view( pointer str, size_type n ) NOEXCEPT :
    base( str, n ) {}
  CONSTEXPR basic_string_view( pointer str ) :
    base( str , str ? string_view_detail::strlen<traits>( str ) : 0 ) {}
  template <typename Allocator> basic_string_view( const std::basic_string<charT, traits, Allocator>& str ) :
    base( str ) {}

#ifdef USE_DEFAULT_DELETE
  CONSTEXPR basic_string_view( const basic_string_view& ) NOEXCEPT = default;
  basic_string_view& operator = ( const basic_string_view& ) NOEXCEPT = default;
#else
  CONSTEXPR basic_string_view( const basic_string_view& other ) NOEXCEPT :
    base( other ) {}
  basic_string_view& operator = ( const basic_string_view& other ) NOEXCEPT {
    static_cast<base&>( *this ) = other;
    return *this;
  }
#endif

  using base::begin;
  using base::end;
  using base::rbegin;
  using base::rend;
  using base::data;
  using base::slice;
  using base::size;

  CONSTEXPR size_type length() const NOEXCEPT { return size(); }

  std::basic_string<charT, traits> to_string() const
  { return std::basic_string<charT, traits>( data(), size() ); }
  template <typename Allocator>
  std::basic_string<charT, traits, Allocator> to_string( Allocator a = Allocator() ) const
  { return std::basic_string<charT, traits, Allocator>( data(), size(), a ); }

  template <typename Allocator>
  /* explicit */ operator std::basic_string<charT, traits, Allocator> () const
  { return to_string<Allocator>(); }

  CONSTEXPR basic_string_view substr( size_type pos = 0, size_type n = npos ) const
  {
    return (pos < size() ?
              slice( pos, std::min( n, size() - pos ) ) :
              (throw std::out_of_range("substr: index out of range")));
  }

  // Three-way compare.
  int compare( const basic_string_view& other ) const
  {
    int result = traits::compare( data(), other.data(), std::min( size(), other.size() ) );
    if ( result == 0 )
      result = static_cast<int>( size() - other.size() );
    return result;
  }
  int compare( const charT* s ) const { return compare( basic_string_view( s ) ); }

  // Same as substr( 0, other.size() ) == other
  bool starts_with( const basic_string_view& other ) const
  { return size() >= other.size() && traits::compare( data(), other.data(), other.size() ) == 0; }

  // Same as substr( size() - other.size(), other.size() ) == other
  bool ends_with( const basic_string_view& other ) const
  { return size() >= other.size() && traits::compare( data() + size() - other.size(), other.data(), other.size() ) == 0; }

  size_type find( charT ) const;
  size_type find( basic_string_view ) const;

  size_type rfind( charT ) const;
  size_type rfind( basic_string_view ) const;

  size_type find_first_of( charT c ) const { return find( c ); }
  size_type find_first_of( basic_string_view ) const;

  size_type find_first_not_of( charT ) const;
  size_type find_first_not_of( basic_string_view ) const;

  size_type find_last_of( charT c ) const { return rfind( c ); }
  size_type find_last_of( basic_string_view ) const;

  size_type find_last_not_of( charT ) const;
  size_type find_last_not_of( basic_string_view ) const;

private:
  CONSTEXPR basic_string_view( base array ) : base( array ) {}

  CONSTEXPR size_type to_pos( const_iterator i ) const
  {
    return (assume( begin() <= i && i <= end() ),
            ( i == end() ) ? npos : i - begin());
  }

  CONSTEXPR size_type to_pos( const_reverse_iterator i ) const
  {
    return (assume( rbegin() <= i && i <= rend() ),
            rend() - i - 1);
  }

  struct comp
  {
    typedef charT first_argument_type;
    typedef charT second_argument_type;
    typedef bool result_type;
    bool operator() ( charT a, charT b ) const
    { return traits::eq( a, b ); }
  };
};

template <typename charT, typename traits>
typename basic_string_view<charT, traits>::size_type
basic_string_view<charT, traits>::find( charT c ) const
{ return to_pos( std::find_if( begin(), end(), std::bind1st( comp(), c ) ) ); }

template <typename charT, typename traits>
typename basic_string_view<charT, traits>::size_type
basic_string_view<charT, traits>::find( basic_string_view s ) const
{ return to_pos( std::search( begin(), end(), s.begin(), s.end(), comp() ) ); }

template <typename charT, typename traits>
typename basic_string_view<charT, traits>::size_type
basic_string_view<charT, traits>::rfind( charT c ) const
{ return to_pos( std::find_if( rbegin(), rend(), std::bind1st( comp(), c ) ) ); }

template <typename charT, typename traits>
typename basic_string_view<charT, traits>::size_type
basic_string_view<charT, traits>::rfind( basic_string_view s ) const
{ return to_pos( std::search( rbegin(), rend(), s.rbegin(), s.rend(), comp() ) ); }

template <typename charT, typename traits>
typename basic_string_view<charT, traits>::size_type
basic_string_view<charT, traits>::find_first_of( basic_string_view<charT, traits> s ) const
{ return to_pos( std::find_first_of( begin(), end(), s.begin(), s.end(), comp() ) ); }

template <typename charT, typename traits>
typename basic_string_view<charT, traits>::size_type
basic_string_view<charT, traits>::find_last_of( basic_string_view<charT, traits> s ) const
{ return to_pos( std::find_first_of( rbegin(), rend(), s.begin(), s.end(), comp() ) ); }

template <typename charT, typename traits>
typename basic_string_view<charT, traits>::size_type
basic_string_view<charT, traits>::find_first_not_of( charT c ) const
{ return to_pos( std::find_if( begin(), end(), std::not1( std::bind1st( comp(), c ) ) ) ); }

namespace string_view_detail {
template <typename InputIterator, typename FwdIterator, typename BinaryPredicate>
InputIterator find_first_not_of( InputIterator pos, InputIterator end,
                                 FwdIterator first, FwdIterator last,
                                 BinaryPredicate cmp )
{
  for ( ; pos != end && std::find_if( first, last, std::bind1st( cmp, *pos ) ) != last; ++pos )
    ;
  return pos;
}
} // namespace string_view_detail

template <typename charT, typename traits>
typename basic_string_view<charT, traits>::size_type
basic_string_view<charT, traits>::find_first_not_of( basic_string_view s ) const
{ return to_pos( string_view_detail::find_first_not_of( begin(), end(), s.begin(), s.end(), comp() ) ); }

template <typename charT, typename traits>
typename basic_string_view<charT, traits>::size_type
basic_string_view<charT, traits>::find_last_not_of( charT c ) const
{ return to_pos( std::find_if( rbegin(), rend(), std::not1( std::bind1st( comp(), c ) ) ) ); }

template <typename charT, typename traits>
typename basic_string_view<charT, traits>::size_type
basic_string_view<charT, traits>::find_last_not_of( basic_string_view s ) const
{ return to_pos( string_view_detail::find_first_not_of( rbegin(), rend(), s.begin(), s.end(), comp() ) ); }

template <typename charT, typename traits>
bool operator == ( basic_string_view<charT, traits> left,
                   basic_string_view<charT, traits> right ) {
  return left.size() == right.size() &&
      std::equal( left.begin(), left.end(), right.begin(), traits::eq );
}

template <typename charT, typename traits>
bool operator == ( basic_string_view<charT, traits> left,
                   typename string_view_detail::identity< basic_string_view<charT, traits> >::type right ) {
  return left.size() == right.size() &&
      std::equal( left.begin(), left.end(), right.begin(), traits::eq );
}
template <typename charT, typename traits>
bool operator == ( typename string_view_detail::identity< basic_string_view<charT, traits> >::type left,
                   basic_string_view<charT, traits> right )
{ return left.size() == right.size() && std::equal( left.begin(), left.end(), right.begin(), traits::eq ); }

template <typename charT, typename traits>
inline bool operator != ( basic_string_view<charT, traits> left,
                          basic_string_view<charT, traits> right )
{ return ! ( left == right ); }
template <typename charT, typename traits>
inline bool operator != ( basic_string_view<charT, traits> left,
                          typename string_view_detail::identity< basic_string_view<charT, traits> >::type right )
{ return ! ( left == right ); }
template <typename charT, typename traits>
inline bool operator != ( typename string_view_detail::identity< basic_string_view<charT, traits> >::type left,
                          basic_string_view<charT, traits> right )
{ return ! ( left == right ); }

template <typename charT, typename traits>
inline bool operator < ( basic_string_view<charT, traits> left,
                         basic_string_view<charT, traits> right )
{ return left.compare( right ) < 0; }
template <typename charT, typename traits>
inline bool operator < ( typename string_view_detail::identity< basic_string_view<charT, traits> >::type left,
                         basic_string_view<charT, traits> right )
{ return left.compare( right ) < 0; }
template <typename charT, typename traits>
inline bool operator < ( basic_string_view<charT, traits> left,
                         typename string_view_detail::identity< basic_string_view<charT, traits> >::type right )
{ return left.compare( right ) < 0; }

template <typename charT, typename traits>
inline bool operator <= ( basic_string_view<charT, traits> left,
                          basic_string_view<charT, traits> right )
{ return left.compare( right ) <= 0; }
template <typename charT, typename traits>
inline bool operator <= ( typename string_view_detail::identity< basic_string_view<charT, traits> >::type left,
                          basic_string_view<charT, traits> right )
{ return left.compare( right ) <= 0; }
template <typename charT, typename traits>
inline bool operator <= ( basic_string_view<charT, traits> left,
                          typename string_view_detail::identity< basic_string_view<charT, traits> >::type right )
{ return left.compare( right ) <= 0; }

template <typename charT, typename traits>
inline bool operator > ( basic_string_view<charT, traits> left,
                         basic_string_view<charT, traits> right )
{ return left.compare( right ) > 0; }
template <typename charT, typename traits>
inline bool operator > ( typename string_view_detail::identity< basic_string_view<charT, traits> >::type left,
                         basic_string_view<charT, traits> right )
{ return left.compare( right ) > 0; }
template <typename charT, typename traits>
inline bool operator > ( basic_string_view<charT, traits> left,
                         typename string_view_detail::identity< basic_string_view<charT, traits> >::type right )
{ return left.compare( right ) > 0; }

template <typename charT, typename traits>
inline bool operator >= ( basic_string_view<charT, traits> left,
                          basic_string_view<charT, traits> right )
{ return left.compare( right ) >= 0; }
template <typename charT, typename traits>
inline bool operator >= ( typename string_view_detail::identity< basic_string_view<charT, traits> >::type left,
                          basic_string_view<charT, traits> right )
{ return left.compare( right ) >= 0; }
template <typename charT, typename traits>
inline bool operator >= ( basic_string_view<charT, traits> left,
                          typename string_view_detail::identity< basic_string_view<charT, traits> >::type right )
{ return left.compare( right ) >= 0; }

namespace string_view_detail {

template <typename T>
class ios_state_setter
{
  typedef typename T::iostate flag_type;
  T& stream;
  flag_type flags;

  ios_state_setter( const ios_state_setter& ); // = delete

public:
  ios_state_setter( T& s, flag_type f = 0 ) : stream( s ), flags( f ) {}
  ~ios_state_setter() { stream.setstate( flags ); }

  flag_type operator & ( flag_type f ) const
  { return flags & f; }

  flag_type operator | ( flag_type f ) const
  { return flags | f; }

  bool operator == ( flag_type f ) const
  { return flags == f; }

  bool operator != ( flag_type f ) const
  { return flags != f; }

  ios_state_setter& operator = ( flag_type f )
  { flags = f; return *this; }

  ios_state_setter& operator |= ( flag_type f )
  { flags |= f; return *this; }

  ios_state_setter& operator &= ( flag_type f )
  { flags &= f; return *this; }
};

} // namespace string_view_detail

template <typename charT, typename traits>
std::basic_ostream<charT, traits>& operator << ( std::basic_ostream<charT, traits>& os, const basic_string_view<charT, traits>& s )
{
  if ( true )
  {
    os.write( s.data(), s.size() );
  }
  else
  {
    typedef std::basic_ostream<charT, traits> ostream;
    typedef basic_string_view<charT, traits> str;

    string_view_detail::ios_state_setter<ostream> state( os, std::ios_base::goodbit );
    const typename ostream::sentry sentry( os );

    if ( unlikely( ! sentry ) )
      state |= std::ios_base::badbit;

    else
    {
      typename str::size_type w = static_cast<typename str::size_type>( std::max( os.width(), static_cast<std::streamsize>( 0 ) ) );
      typename str::size_type pad = w <= s.size() ? 0 : w - s.size();
      std::basic_streambuf<charT, traits>* buf = os.rdbuf();
      charT f = os.fill();

      if ( pad > 0 && ( os.flags() & std::ios_base::adjustfield ) != std::ios_base::left )
      {
        for (; pad > 0 ; --pad )
        {
          if ( traits::eq_int_type( buf -> sputc( f ), traits::eof() ) )
          {
            state |= std::ios_base::badbit;
            break;
          }
        }
      }
      if ( state == std::ios_base::goodbit )
      {
        if ( buf -> sputn( s.data(), s.size() ) != static_cast<std::streamsize>( s.size() ) )
          state |= std::ios_base::badbit;
      }
      if ( pad > 0 && state == std::ios_base::goodbit )
      {
        for (; pad > 0 ; --pad )
        {
          if ( traits::eq_int_type( buf -> sputc( f ), traits::eof() ) )
          {
            state |= std::ios_base::badbit;
            break;
          }
        }
      }
      os.width( 0 );
    }
  }

  return os;
}

template <typename charT, typename traits, typename Allocator>
std::basic_string<charT, traits, Allocator> to_string( const basic_string_view<charT, traits>& s,
                                                       const Allocator& a = Allocator() )
{ return std::basic_string<charT, traits, Allocator>( s.begin(), s.end(), a ); }

typedef basic_string_view<char> string_view;
typedef basic_string_view<wchar_t> wstring_view;
#if 0
typedef basic_string_view<char16_t> u16string_view;
typedef basic_string_view<char32_t> u32string_view;
#endif

int stoi( const string_view& s, std::size_t* index = 0, int base = 10 );
inline int stoi( const char* s, std::size_t* index = 0, int base = 10 )
{ return stoi( string_view( s ), index, base ); }

long stol( const string_view& s, std::size_t* index = 0, int base = 10 );
inline long stol( const char* s, std::size_t* index = 0, int base = 10 )
{ return stol( string_view( s ), index, base ); }

unsigned long stoul( const string_view& s, std::size_t* index = 0, int base = 10 );
inline unsigned long stoul( const char* s, std::size_t* index = 0, int base = 10 )
{ return stoul( string_view( s ), index, base ); }

long long stoll( const string_view& s, std::size_t* index = 0, int base = 10 );
inline long long stoll( const char* s, std::size_t* index = 0, int base = 10 )
{ return stoll( string_view( s ), index, base ); }

unsigned long long stoull( const string_view& s, std::size_t* index = 0, int base = 10 );
inline unsigned long long stoull( const char* s, std::size_t* index = 0, int base = 10 )
{ return stoull( string_view( s ), index, base ); }

float stof( const string_view& s, std::size_t* index = 0 );
inline float stof( const char* s, std::size_t* index = 0 )
{ return stof( string_view( s ), index ); }

double stod( const string_view& s, std::size_t* index = 0 );
inline double stod( const char* s, std::size_t* index = 0 )
{ return stod( string_view( s ), index ); }

long double stold( const string_view& s, std::size_t* index = 0 );
inline long double stold( const char* s, std::size_t* index = 0 )
{ return stold( string_view( s ), index ); }

int stoi( const wstring_view& s, std::size_t* index = 0, int base = 10 );
inline int stoi( const wchar_t* s, std::size_t* index = 0, int base = 10 )
{ return stoi( wstring_view( s ), index, base ); }

long stol( const wstring_view& s, std::size_t* index = 0, int base = 10 );
inline long stol( const wchar_t* s, std::size_t* index = 0, int base = 10 )
{ return stol( wstring_view( s ), index, base ); }

unsigned long stoul( const wstring_view& s, std::size_t* index = 0, int base = 10 );
inline unsigned long stoul( const wchar_t* s, std::size_t* index = 0, int base = 10 )
{ return stoul( wstring_view( s ), index, base ); }

long long stoll( const wstring_view& s, std::size_t* index = 0, int base = 10 );
inline long long stoll( const wchar_t* s, std::size_t* index = 0, int base = 10 )
{ return stoll( wstring_view( s ), index, base ); }

unsigned long long stoull( const wstring_view& s, std::size_t* index = 0, int base = 10 );
inline unsigned long long stoull( const wchar_t* s, std::size_t* index = 0, int base = 10 )
{ return stoull( wstring_view( s ), index, base ); }

float stof( const wstring_view& s, std::size_t* index = 0 );
inline float stof( const wchar_t* s, std::size_t* index = 0 )
{ return stof( wstring_view( s ), index ); }

double stod( const wstring_view& s, std::size_t* index = 0 );
inline double stod( const wchar_t* s, std::size_t* index = 0 )
{ return stod( wstring_view( s ), index ); }

long double stold( const wstring_view& s, std::size_t* index = 0 );
inline long double stold( const wchar_t* s, std::size_t* index = 0 )
{ return stold( wstring_view( s ), index ); }


namespace std {
#ifdef USE_TR1
namespace tr1 {
#endif
template <typename charT, typename traits>
struct hash< basic_string_view<charT, traits> > {
  std::size_t operator () ( basic_string_view<charT, traits> s ) const
  { return s.hash(); }
};
#ifdef USE_TR1
} // namespace tr1
#endif
} // namespace std

#endif // STRING_VIEW_HPP
