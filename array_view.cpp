#include "array_view.hpp"

#include <iostream>

namespace array_view_detail {
#ifndef USE_CONSTEXPR
std::size_t
hash_characters( array_view<unsigned char> array )
{
  // Stolen from Folly!
  //
  // Taken from fbi/nstring.h:
  //    Quick and dirty bernstein hash...fine for short ascii strings
  std::size_t hash = 5381;
  for (array_view<unsigned char>::size_type i = 0; i < array.size(); ++i) {
    hash = 33 * hash + array[i];
  }
  return hash;
}
#endif
} // namespace array_view_detail
